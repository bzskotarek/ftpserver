#define MAX_SIZE 20
#define DEFAULT_SERVER_PORT 21
#define DEFAULT_TIMEOUT 600

#include <boost/lexical_cast.hpp>
#include "server.h"
#include "cmdthread.h"

using namespace std;

struct WaitingForEndOfCmdLoopArgument {
    CmdThread *cmdThread;
    pthread_t threadCmdMainLoop;
};

// watek oczekujacy na zakonczenie komunikacji z klientem
// klient przeslal polecenie 'QUIT', utrata polaczenia lub przekroczony timeout
void *threadWaitingForEndOfCmdLoop(void *arg) {
    WaitingForEndOfCmdLoopArgument *wfecla = (WaitingForEndOfCmdLoopArgument*)arg;

    pthread_join(wfecla->threadCmdMainLoop, nullptr);

    cout << "LOG: Command thread has terminated." << endl;

    delete wfecla->cmdThread;
    delete wfecla;

    return nullptr;
}

// wyselekcjonowanie wartosci dla portu i timeoutu oraz sprawdzenie poprawnosci polecenia
bool analyseParameters(const int &argc, char **argv, int &port, int &timeout) {
    for(int i = 0; i < argc; i++) {
        string parameter(argv[i]);

        if(parameter == "-t") {
            if(i == argc-1)
                return false;

            if(isNumber(string(argv[i+1])))
                timeout = boost::lexical_cast<int>(argv[i+1]);
            else
                return false;
        } else if(parameter == "-p") {
            if(i == argc-1)
                return false;

            if(isNumber(string(argv[i+1])))
                port = boost::lexical_cast<int>(argv[i+1]);
            else
                return false;
        }
    }

    return true;
}

int main(int argc, char **argv) {
    int port = DEFAULT_SERVER_PORT;
    int timeout = DEFAULT_TIMEOUT;

    // jezeli argc == 1 to stosujemy wartosci domyslne
    if(argc != 1) {
        if(!analyseParameters(argc, argv, port, timeout)) {
            cout << "Syntax error." << endl;
            cout << "Use '-p' to pass port and '-t' to pass timeout in seconds." << endl;
            return EXIT_FAILURE;
        }
    }

    CmdThread::timeoutLimit = timeout;
    Server server(port);
    server.createSocketAndBind();
    server.listen(MAX_SIZE);

    while(1) {
        int clientSocket = server.accept();

        // klasa, ktora obsluguje cala komunikacje z klientem
        CmdThread *ct = new CmdThread(clientSocket);

        // watek, ktory oczekuje na zakonczenie watku klienta, aby zwolnic po nim zasoby
        WaitingForEndOfCmdLoopArgument *wfea = new WaitingForEndOfCmdLoopArgument;
        wfea->cmdThread = ct;
        wfea->threadCmdMainLoop = ct->runThreads();
        pthread_t thread;
        pthread_create(&thread, nullptr, threadWaitingForEndOfCmdLoop, wfea);
    }
}
