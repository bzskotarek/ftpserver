#ifndef SERVER_H
#define SERVER_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <string>
#include <strings.h>
#include <iostream>

using namespace std;

class Server
{
public:
    Server(ushort servicePort);
    ~Server();

    ushort getPort();
    int createSocketAndBind();
    void listen(int max);
    int accept();

protected:
    struct sockaddr_in serverAddr;
    int serverSocket;
    ushort port;

    void init(ushort servicePort);
};

#endif // SERVER_H
