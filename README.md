# FTPServerSimple

## Budowanie
Program można skompilować używając na przykład jednego z dwóch niżej wymienionych sposobów:
* Używając cmake
```
cmake CmakeLists.txt &&
make
```
* Lub korzystając z g++
```
g++ -std=c++11 -lpthread -lcrypt cmdthread.cpp datathread.cpp main.cpp server.cpp utilityfunctions.cpp
```

## Sposób realizacji oraz implementacji
Serwer został napisany w języku C++ stosując interfejs gniazd BSD oraz bibliotekę pthread. Dodatkowo użyte zostały następujące biblioteki:

* **crypt** System autoryzacji użytkowników oparty został na pliku /etc/shadow, który zawiera hasła zaszyfrowane przy pomocy SHA-512. Aby móc dokonać weryfikacji poprawności hasła, użyta została funkcja crypt, która przy pomocy funkcji haszującej generuje sumę kontrolną. Suma kontrolna wygenerowana w ten sposób zostaje porównana do tej zawartej w pliku /etc/shadow i w przypadku zgodności, serwer umożliwia dostęp do zasobów danego użytkownika.


* **boost** W programie zostało użytych kilka funkcji z tej biblioteki, aby nie produkować dodatkowych linii kodu, które nie są bezpośrednio związane z działaniem serwera.

Ze względu na dostęp do pliku /etc/shadow, serwer wymaga uprawnień administratora. Jednak pełne uprawnienia wykorzystywane są tylko przy jego obsłudze. Pozostałe polecenia (ls, stat itd.) wykonywane są z wykorzystaniem komendy su na rzecz danego użytkownika używając uprawnień go charakteryzujących.

## Opis zawartości plików
* **server.h i server.cpp** Klasa, która nasłuchuje i przyjmuje klientów do systemu.
* **cmdthread.h i cmdthread.cpp** Klasa, która zajmuje się komunikacją z klientem. Odbiera wszystkie polecenia, przetwarza i wysyła wiadomości zwrotne. Inicjalizuje transfer danych.
* **datathread.h i datathread.cpp** Klasa, która jest szczególnym przypadkiem klasy Server, ponieważ nasłuchuje tylko jednego klienta. Obsługuje ona transfer danych, czyli zajmuje się przesłaniem lub odebraniem rezultatów funkcji STOR, RERT i LIST.
* **utilityfunctions.h i utilityfunctions.cpp** Zbiór funkcji, które zajmują się pobraniem rezultatów poleceń na rzecz danego użytkownika, generowaniem liczb, pobieraniem adresu IP dla danego interfejsu, sprawdzeniem czy string jest liczbą oraz weryfikacją poprawności hasła dla użytkownika.

## Obsługa programu
Aplikacje można uruchomić używając parametrów:
* **-p LICZBA** Określenie numeru portu. Domyślnie 21.
* **-t LICZBA** Określenie limitu czasu bezczynności w sekundach, po którym nastąpi rozłączenie z serwerem. Domyślnie 600.

## Lista zaimplementowanych komend
* USER
* PASS
* SYST
* MKD
* RMD
* DELE
* SIZE
* RNFR
* RNTO
* SITE CHMOD
* CWD
* PWD
* CDUP
* REST
* ABOR
* LIST
* RETR
* STOR
* TYPE
* PASV
* FEAT
* NOOP
 
