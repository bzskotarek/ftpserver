#ifndef UTILITYFUNCTIONS_H
#define UTILITYFUNCTIONS_H

#define SUDO string("")

#define END_OF_STRING "\r\n"

#include <stdio.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <string>
#include <fstream>
#include <random>
#include <exception>
#include <iostream>
#include <crypt.h>

using namespace std;

int generateValue(int min, int max);
string getMachineAddress();
string getResultOfSystemCommand(const string &user, const string &command, bool addEndOfString);
bool isPasswordCorrect(const string &user, const string &password);
bool isNumber(const string &s);

#endif // UTILITYFUNCTIONS_H
