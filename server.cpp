#include "server.h"

Server::Server(ushort servicePort) {
    init(servicePort);
}

Server::~Server() {
    close(serverSocket);
}

ushort Server::getPort() {
    return port;
}

void Server::init(ushort servicePort) {
    bzero(&serverAddr, sizeof serverAddr);
    serverAddr.sin_addr.s_addr = INADDR_ANY;
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(servicePort);
    port = servicePort;
}

int Server::createSocketAndBind() {
    if((serverSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        cout << "ERROR: Could not create the socket" << endl;

        exit(EXIT_FAILURE);
    }

    // zapobieganie temu, ze serwer nie dziala, a port zablokowany
    int opt = 1;
    setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof opt);

    if(bind (serverSocket, (struct sockaddr*) &serverAddr, sizeof serverAddr) < 0) {
        cout << "ERROR: Could not bind the socket " << serverSocket << " to " << port << " port" << endl;

        return -1;
    }

    return 0;
}

void Server::listen(int max) {
    if(::listen (serverSocket, max) < 0) {
        cout << "ERROR: Cannot listen" << endl;

        exit(EXIT_FAILURE);
    }
}

int Server::accept() {
    struct sockaddr_in clientAddr;
    int clientSocket, clientLength;

    clientLength = sizeof(struct sockaddr_in);
    if((clientSocket = ::accept (serverSocket, (struct sockaddr*) &clientAddr, (socklen_t*) &clientLength)) < 0) {
        cout << "ERROR: Could not connect with client" << endl;

        exit(EXIT_FAILURE);
    }

    return clientSocket;
}

