#include "datathread.h"

DataThread::DataThread() : Server(generateValue(PORT_EPHERMAL_LOW, PORT_EPHERMAL_HIGH)), condWaitingForEnd(PTHREAD_COND_INITIALIZER),
    mutexWaitingForEnd(PTHREAD_MUTEX_INITIALIZER), mutexStopTransfer(PTHREAD_MUTEX_INITIALIZER), running(true), error(false), abor(false) {
    // przypisywanie portu efemerycznego do gniazda. Maksymalnie 10 prob. Przy kazdej nieudanej nowy jest generowany nowy port
    bool good = false;
    int counter = 0;
    while(good == false && counter < MAX_TRIES) {
        if(createSocketAndBind() == -1) {
            cout << "ERROR: Couldn't bind the socket " << serverSocket << " to " << port << " port. Attempt (" << counter << "/" << MAX_TRIES << ")" << endl;

            counter++;
            init(generateValue(PORT_EPHERMAL_LOW, PORT_EPHERMAL_HIGH));

            continue;
        }
        good = true;
    }

    if(counter == MAX_TRIES)
        exit(EXIT_FAILURE);

    listen(1);

    if(good == false) {
        cout << "ERROR: The program terminated." << endl;
        exit(EXIT_FAILURE);
    }
}

DataThread::~DataThread() {
    pthread_mutex_destroy(&mutexStopTransfer);
    pthread_mutex_destroy(&mutexWaitingForEnd);
    pthread_cond_destroy(&condWaitingForEnd);
}

bool DataThread::isRunning() {
    return running;
}

bool DataThread::isError() {
    return error;
}

string DataThread::getCommand() {
    return command;
}

pthread_cond_t *DataThread::getCondWaitingForEnd() {
    return &condWaitingForEnd;
}

pthread_mutex_t *DataThread::getMutexWaitingForEnd() {
    return &mutexWaitingForEnd;
}

void DataThread::setAbor(bool abor) {
    pthread_mutex_lock(&mutexStopTransfer);
    this->abor = abor;
    pthread_mutex_unlock(&mutexStopTransfer);
}

// funkcja zapisujaca parametry transferu oraz tworzaca watek dla niego. Wywoluja ja polecenia RETR oraz STOR
void DataThread::executeAndWrite(const string &command, const string &fileName, TYPE typ, const string &path, int offset) {
    this->command = command;
    this->commandParameterOrFileName = fileName;
    this->typ = typ;
    this->path = path;
    this->offset = offset;

    pthread_create(&threadId, nullptr, thread, this);
}

// funkcja zapisujaca parametry transferu oraz tworzaca watek dla niego. Wywoluje ja polecenie LIST
void DataThread::executeAndWrite(const string &command, const string &parameter, TYPE typ, const string &path, const string &login) {
    this->command = command;
    this->commandParameterOrFileName = parameter;
    this->typ = typ;
    this->path = path;
    this->login = login;

    pthread_create(&threadId, nullptr, thread, this);
}

void *DataThread::thread(void *arg) {
    DataThread *dt = (DataThread*)arg;

    // czekanie tylko na jednego klienta
    dt->dataSocket = dt->accept();

    cout << "LOG: Client (data connection) - the client connected" << endl;
    if(dt->command == "LIST") {
        commandLIST(dt, dt->login);
    } else if (dt->command == "RETR") {
        sendFile(dt->dataSocket, dt->path + "/" + dt->commandParameterOrFileName, dt->typ, dt->offset, dt->abor, dt->mutexStopTransfer, dt->error);
    } else if (dt->command == "STOR") {
        receiveFile(dt->dataSocket, dt->path + "/" + dt->commandParameterOrFileName, dt->typ, dt->offset, dt->abor, dt->mutexStopTransfer, dt->error);
    }

    close(dt->dataSocket);
    close(dt->serverSocket);

    cout << "LOG: Client (data connection) - the server finished " << dt->command << " " << dt->commandParameterOrFileName << endl;

    // generowanie sygnalu dla watku, ktory oczekuje na zakonczenie transferu danych z klientem
    pthread_mutex_lock(&dt->mutexWaitingForEnd);
    dt->running = false;
    pthread_cond_signal(&dt->condWaitingForEnd);
    pthread_mutex_unlock(&dt->mutexWaitingForEnd);

    return nullptr;
}

// funkcja, ktora wysyla plik do klienta z uwzglednieniem ewentualnego wymuszenia przerwania transferu
void DataThread::sendFile(int dataSocket, string filePath, TYPE typ, int offset, bool &abor, pthread_mutex_t &mutexStopTransfer, bool &error) {
    ifstream file;

    if(typ == BINARY) {
        file.open(filePath, ios::binary);

        if(!file.is_open()) {
            error = true;
            return;
        }

        file.seekg(offset, ios_base::beg);

        char bufor[BUFOR_SIZE];
        do {
            pthread_mutex_lock(&mutexStopTransfer);
            if(abor == true)
                break;
            pthread_mutex_unlock(&mutexStopTransfer);

            file.read(bufor, BUFOR_SIZE);

            write(dataSocket, bufor, file.gcount());
        } while(file.gcount());
    } else if(typ == ASCII) {
        file.open(filePath);

        if(!file.is_open()) {
            error = true;
            return;
        }

        file.seekg(offset, ios_base::beg);

        string content("");
        string line;
        while(getline(file, line)) {
            pthread_mutex_lock(&mutexStopTransfer);
            if(abor == true)
                break;
            pthread_mutex_unlock(&mutexStopTransfer);

            content = line + END_OF_STRING;
            write(dataSocket, content.c_str(), content.length());
        }
    }

    file.close();
}

// funkcja, ktora odbiera plik od klienta z uwzglednieniem ewentualnego wymuszenia przerwania transferu
void DataThread::receiveFile(int dataSocket, string filePath, TYPE typ, int offset, bool &abor, pthread_mutex_t &mutexStopTransfer, bool &error) {
    ofstream file;

    char bufor[BUFOR_SIZE];
    int length;
    if(typ == BINARY) {
        file.open(filePath, ios::binary);

        if(!file.is_open()) {
            error = true;
            return;
        }

        file.seekp(offset, ios_base::beg);

        while((length = read(dataSocket, bufor, BUFOR_SIZE)) > 0) {
            pthread_mutex_lock(&mutexStopTransfer);
            if(abor == true)
                break;
            pthread_mutex_unlock(&mutexStopTransfer);

            file.write(bufor, length);
        }
    } else if (typ == ASCII) {
        string content("");
        file.open(filePath);

        if(!file.is_open()) {
            error = true;
            return;
        }

        file.seekp(offset, ios_base::beg);

        while((length = read(dataSocket, bufor, BUFOR_SIZE)) > 0) {
            pthread_mutex_lock(&mutexStopTransfer);
            if(abor == true)
                break;
            pthread_mutex_unlock(&mutexStopTransfer);

            content += bufor;
        }

        // zamiana wszystkich \r\n na charakterystyczne dla danego systemu
        boost::replace_all(content, END_OF_STRING, END_OF_STRING_LINUX);

        file.write(content.c_str(), content.length());
    }

    file.close();
}

// funkcja wysylajaca do klienta rezultat polecenia LS. LANG=en_GB zostalo uzyte w celu uzyskania formatu daty
// akceptowanego przez klientow
void DataThread::commandLIST(DataThread *dt, const string &login) {
    string command;
    if(dt->commandParameterOrFileName.length() > 0)
        command = "LANG=en_GB ls -aln --time-style=\"+%b %d %H:%M\"";
    else
        command = "LANG=en_GB ls -ln --time-style=\"+%b %d %H:%M\"";
    string toExecute = command + " \"" + dt->path + "\" | tail -n +2 "; // tail zeby usunac zbedny naglowek

    string result;
    try {
        result = getResultOfSystemCommand(login, toExecute, true);
    }
    catch(exception &) {
        dt->error = true;
        return;
    }

    write(dt->dataSocket, result.c_str(), result.length());
}
