#include "cmdthread.h"

vector<DataThread*> CmdThread::unusedDataThreads;
vector<DataThread*> CmdThread::usedDataThreads;
int CmdThread::timeoutLimit;

CmdThread::CmdThread(int cmdSocket) : socket(cmdSocket), counter(0), mutexCounter(PTHREAD_MUTEX_INITIALIZER),
    loginIn(false), login(""), path(""), receivedCommand(""), toWrite(""), type(UNKNOWN), REST(0) { }

CmdThread::~CmdThread() {
    pthread_mutex_destroy(&mutexCounter);
}

// kazdy klient dziala w odzielnym watku. Jeden z nich obsluguje komunikacje z klientem, a drugi liczy czasy
// i sprawdza czy klient nie przekroczyl czasu bezczynnosci
pthread_t CmdThread::runThreads() {
    pthread_create(&threadID, nullptr, thread, this);

    TimeoutGuardianArgument *arg = new TimeoutGuardianArgument;
    arg->mutexCounter = &mutexCounter;
    arg->counter = &counter;
    arg->socket = socket;
    arg->threadId = threadID;
    pthread_create(&threadTimeoutGuardianID, nullptr, threadTimeoutGuardian, arg);
    return threadID;
}

// watek oblugujacy cala komunikacje z klientem
void *CmdThread::thread(void *arg) {
    CmdThread *ct = (CmdThread*)arg;

    log(ct->socket, "new client connected");
    writeAndLog(ct->socket, "220 Welcome to FTP service."); // wiadomosc powitalna

    bool isEnd = false;
    while(!isEnd) {
        // funkcja receiveMessage() zwraca false kiedy zostanie utracone polaczenie z klientem. Wowczas konczymy watek
        if(!ct->receiveMessage()) {
            isEnd = true;
        } else {
            // kazda wiadomosc zostaje przekonwertowana do wielkich znakow, zeby wielkosc liter nie miala znaczenia
            for(unsigned int i = 0; i < ct->receivedCommand.length(); i++) {
                ct->receivedCommand[i] = toupper(ct->receivedCommand[i]);
            }

            if(ct->receivedCommand == "QUIT") {
                ct->writeAndLog(ct->socket, "221 Goodbye.");
                close(ct->socket);
                isEnd = true;
            } else {
                // polecenia mozna podzielic na dwa rodzaje. Te, ktore oblugiwane sa kiedy klient nie jest zalogowany
                // i te kiedy jest zalogowany
                if(ct->loginIn == false) {
                    ct->notLoggedIn();
                } else {
                    ct->loggedIn();
                }
            }

            // zawsze zerujemy licznik po otrzymaniu jakiegos polecenia, poniewaz
            // klient wykazal aktywnosc
            pthread_mutex_lock(&ct->mutexCounter);
            ct->counter = 0;
            pthread_mutex_unlock(&ct->mutexCounter);
        }
    }

    // zatrzymanie watku od timeoutu, poniewaz nie jest scisle powiazany z tym watkiem
    // i dzialal by mimo braku polaczenia z klientem
    pthread_cancel(ct->threadTimeoutGuardianID);

    return nullptr;
}

// funkcja oczekujaca na zakonczenie przesylania danych do klienta. Tworzona jest za kazdym razem
// gdy klient przesle polecenie PASV i trwa az do zakonczenia transferu
void *CmdThread::threadWaitingForEndOfDataThread(void *arg) {
    WaitingForEndArgument *ga = (WaitingForEndArgument *)arg;

    pthread_mutex_t *mutex = ga->dataThread->getMutexWaitingForEnd();
    pthread_cond_t *cond = ga->dataThread->getCondWaitingForEnd();

    // zmienna warunkowa. Oczekiwanie na koniec watku
    pthread_mutex_lock(mutex);
    while(ga->dataThread->isRunning())
        pthread_cond_wait(cond, mutex);
    pthread_mutex_unlock(mutex);

    log(*(ga->cmdSocket), "guardian detected end of data transfer");

    // reakcja na zakonczenie przesylania danych. Jezeli nie wystapil zaden blad
    // przesylamy komunikatu o sukcesie. W przeciwnym razie o niepowodzeniu
    if(ga->dataThread->isError()) {
        if(ga->dataThread->getCommand() == "LIST")
            writeAndLog(*(ga->cmdSocket), "500 Failed to list directory.");
        else if(ga->dataThread->getCommand() == "RETR") {
            writeAndLog(*(ga->cmdSocket), "550 Failed to open file.");
        }     else if(ga->dataThread->getCommand() == "STOR") {
            writeAndLog(*(ga->cmdSocket), "550 Failed to open file.");
        }
    } else {
        if(ga->dataThread->getCommand() == "LIST")
            writeAndLog(*(ga->cmdSocket), "226 Directory send OK.");
        else if(ga->dataThread->getCommand() == "RETR") {
            writeAndLog(*(ga->cmdSocket), "226 Transfer RETR complete.");
        }     else if(ga->dataThread->getCommand() == "STOR") {
            writeAndLog(*(ga->cmdSocket), "226 Transfer STOR complete.");
        }
    }

    // usuwanie tego watku z listy aktywnych watkow
    for(unsigned int i = 0; i < usedDataThreads.size(); i++) {
        if(usedDataThreads[i] == ga->dataThread) {
            usedDataThreads.erase(usedDataThreads.begin() + i);
            break;
        }
    }

    delete ga->dataThread;
    ga->dataThread = nullptr;
    delete ga;

    return nullptr;
}

// watek, ktory pilnuje czy uzytkownik przekroczyl timeout
void *CmdThread::threadTimeoutGuardian(void *arg) {
    TimeoutGuardianArgument *tassa = (TimeoutGuardianArgument*)arg;

    int *counter = tassa->counter;
    pthread_mutex_t *mutexCounter = tassa->mutexCounter;
    int socket = tassa->socket;
    pthread_t threadID = tassa->threadId;

    // zwalniamy pamiec na poczatku, poniewaz moze sie zdazyc, ze wyjscie z petli glownej nastapi w skutek utraty polaczenia
    // z klientem. Wowczas jezeli 'delete tassa' byloby na koncu funkcji, to przy wywolaniu 'pthread_cancel' w petli glownej
    // pamiec nie zostala by zwolniona.
    delete tassa;

    bool exit = false;
    while(!exit) {
        sleep(1);

        pthread_mutex_lock(mutexCounter);
        *(counter) = *(counter) + 1;
        if(*(counter) == timeoutLimit)
            exit = true;
        pthread_mutex_unlock(mutexCounter);
    }

    // musimy w tym watku zakonczyc dzialanie watku glownego, poniewaz watek glowny tkwi w funkcji 'read()' i interakcja nie
    // jest mozliwa
    writeAndLog(socket, "421 Timeout.");
    pthread_cancel(threadID);

    return nullptr;
}

// obluga polecen dla uzytkownika, ktory jeszcze sie nie zalogowal
void CmdThread::notLoggedIn() {
    if(receivedCommand == "USER") {
        login = receivedParameter; // USER xxxxx (od 5-tego znaku)
        writeAndLog(socket, "331 Please specify the password.");
    } else if(receivedCommand == "PASS") {
        if(login.empty())
            writeAndLog(socket, "503 Login with USER first.");
        else {
            if(isPasswordCorrect(login, receivedParameter)) {
                loginIn = true;
                path = "/home/" + login;
                writeAndLog(socket, "230 Login successful.");
            } else
                writeAndLog(socket, "530 Login incorrect.");
        }
    } else //if(login.empty())
        writeAndLog(socket, "530 Please login with USER and PASS.");
}

// obluga polecen dla uzytkownika, ktory zalogowal sie pomyslnie
void CmdThread::loggedIn() {
    if(receivedCommand == "SYST")
        writeAndLog(socket, "215 UNIX Type: L8");
    else if(receivedCommand == "MKD") {
        try {
            getResultOfSystemCommand(login, "mkdir \"" + filePath(receivedParameter) + "\"", false);
        } catch(exception &) {
            writeAndLog(socket, "550 Create directory operation failed.");
            return;
        }
        writeAndLog(socket, "257 \"" + receivedParameter + "\" created");
    } else if(receivedCommand == "RMD") {
        try {
            getResultOfSystemCommand(login, "rm -r \"" + filePath(receivedParameter) + "\"", false);
        } catch(exception &) {
            writeAndLog(socket, "550 Remove operation failed.");
            return;
        }
        writeAndLog(socket, "250 Remove directory operation successful.");
    } else if(receivedCommand == "DELE") {
        try {
            getResultOfSystemCommand(login, "rm -r \"" + filePath(receivedParameter) + "\"", false);
        } catch(exception &) {
            writeAndLog(socket, "550 Delete operation failed.");
            return;
        }
        writeAndLog(socket, "250 Delete operation successful.");
    } else if(receivedCommand == "SIZE") {
        if(receivedParameter.length() == 0)
            writeAndLog(socket, "550 Could not get file size.");
        else {
            string result;
            try {
                result = getResultOfSystemCommand(login, "stat -c %s \"" + filePath(receivedParameter) + "\"", false);
            } catch(exception &) {
                writeAndLog(socket, "550 Could not get file size.");
                return;
            }

            writeAndLog(socket, "213 " + result);
        }
    } else if(receivedCommand == "RNFR") {
        toRename = receivedParameter;
        writeAndLog(socket, "350 Ready for RNTO.");
    } else if (receivedCommand == "RNTO") {
        try {
            getResultOfSystemCommand(login, "mv \"" + filePath(toRename) + "\" \"" + filePath(receivedParameter) + "\"", false);
        } catch(exception &) {
            toRename.clear();
            writeAndLog(socket, "550 RNFR or RNTO command failed.");
            return;
        }
        toRename.clear();
        writeAndLog(socket, "250 Rename successful.");
    } else if(receivedCommand == "SITE") {
        // tylko chmody
        string internalCommand = receivedParameter.substr(0, receivedParameter.find_first_of(' '));

        if(internalCommand == "CHMOD") {
            string parameter = receivedParameter.substr(receivedParameter.find_first_of(' ') + 1);
            string numeric = parameter.substr(0, parameter.find_first_of(' '));
            string nameOfFile = fileWithoutPath(parameter.substr(parameter.find_first_of(' ') + 1));

            try {
                getResultOfSystemCommand(login, "chmod " + numeric + " \"" + path + "/" + nameOfFile + "\"", false);
            } catch(exception &) {
                writeAndLog(socket, "550 SITE CHMOD command failed.");
                return;
            }

            writeAndLog(socket, "200 SITE CHMOD command ok.");
        } else
            writeAndLog(socket, "500 Unknown SITE command.");
    } else if(receivedCommand == "CWD") {
        if(changeDir(receivedParameter))
            writeAndLog(socket, "250 Directory successfully changed.");
        else
            writeAndLog(socket, "550 Failed to change directory.");
    } else if(receivedCommand == "PWD")
        writeAndLog(socket, "257 \"" + path + "\"");
    else if(receivedCommand == "CDUP") {
        if(changeDir(".."))
            writeAndLog(socket, "250 Directory successfully changed.");
        else
            writeAndLog(socket, "550 Failed to change directory.");
    } else if(receivedCommand == "REST") {
        REST = boost::lexical_cast<int>(receivedParameter);
        writeAndLog(socket, "350 Restart position accepted (" + receivedParameter + ").");
    } else if(receivedCommand == "ABOR") {
        bool moreThanZero = false;

        // przerywanie wszystkich aktywnych transferow
        while(usedDataThreads.size()) {
            usedDataThreads[0]->setAbor(true);
            usedDataThreads.erase(usedDataThreads.begin());
            moreThanZero = true;
        }
        if(!moreThanZero)
            writeAndLog(socket, "225 No transfer to ABOR.");
    } else if (receivedCommand == "LIST") {
        DataThread *dataThread = getUnusedThread();

        if(dataThread != nullptr) {
            writeAndLog(socket, "150 Here comes the directory listing.");
            dataThread->executeAndWrite(receivedCommand, receivedParameter, ASCII, path, login);
        } else
            writeAndLog(socket, "425 Use PORT or PASV first.");
    } else if(receivedCommand == "RETR") {
        DataThread *dataThread = getUnusedThread();

        string result;
        try {
            result = getResultOfSystemCommand(login, "stat -c %s \"" + filePath(receivedParameter) + "\"", false);
        } catch(exception &) {
            writeAndLog(socket, "550 File not found.");
            return;
        }

        if(dataThread != nullptr) {
            writeAndLog(socket, "150 Opening  " + enumStrings[type] + " mode data connection for " + receivedParameter + " (" +
                        result + " bytes).");
            dataThread->executeAndWrite(receivedCommand, fileWithoutPath(receivedParameter), type, path, REST);
            REST = 0;
        } else
            writeAndLog(socket, "425 Use PORT or PASV first.");
    } else if(receivedCommand == "STOR") {
        DataThread *dataThread = getUnusedThread();

        if(dataThread != nullptr) {
            dataThread->executeAndWrite(receivedCommand, fileWithoutPath(receivedParameter), type, path, REST);
            REST = 0;
            writeAndLog(socket, "150 Ok to send data.");
        } else
            writeAndLog(socket, "425 Use PORT or PASV first.");
    } else if(receivedCommand == "TYPE") {
        if(receivedParameter == "I") {
            type = BINARY;
            writeAndLog(socket, "200 Switching to binary mode.");
        } else if(receivedParameter == "A") {
            type = ASCII;
            writeAndLog(socket, "200 Switching to ASCII mode.");
        } else {
            type = UNKNOWN;
            writeAndLog(socket, "500 Unrecognised TYPE command.");
        }
    } else if(receivedCommand == "PASV") {
        addDataThread();
    } else if(receivedCommand == "FEAT") {
        writeAndLog(socket, "211-Features");
        writeAndLog(socket, "PASV");
        writeAndLog(socket, "REST STREAM");
        writeAndLog(socket, "SIZE");
        writeAndLog(socket, "UTF8");
        writeAndLog(socket, "211 End");
    } else if(receivedCommand == "NOOP")
        writeAndLog(socket, "200 NOOP ok.");
    else
        writeAndLog(socket, "501 Option not understood.");
}

// funkcja, ktora tworzy komunikacje z klientem w trybie pasywnym
DataThread *CmdThread::getPASV() {
    string address = getMachineAddress();

    vector<string> strings;
    boost::split(strings, address, boost::is_any_of("."));

    // stworzenie sewera klienta, ktorym bedzie przesylal dane
    DataThread *dataThread = new DataThread();

    WaitingForEndArgument *ga = new WaitingForEndArgument;
    ga->cmdSocket = &socket;
    ga->dataThread = dataThread;

    pthread_create(&threadWaitingForEndID, nullptr, threadWaitingForEndOfDataThread, ga);
    // podzielenie portu na p1 i p2. Gdzie p1 to x/256 a p2 to reszta z tego dzielenia
    int port = dataThread->getPort();
    int p1 = port/256;
    int p2 = port % 256;

    strings.push_back(to_string(p1));
    strings.push_back(to_string(p2));

    string pasvMessage("");
    for(string item : strings) {
        pasvMessage += item + ",";
    }
    pasvMessage = pasvMessage.substr(0, pasvMessage.length()-1);

    writeAndLog(socket, "227 Entering Passive Mode (" + pasvMessage + ")");

    return dataThread;
}

// funkcja dodajaca stworzony obiekt klasy obslugi transferu do aktywnych transferow
void CmdThread::addDataThread() {
    DataThread *dataThread = getPASV();

    unusedDataThreads.push_back(dataThread);
}

// funkcja, ktora zwraca obiekt klasy, ktora obsluguje transfer. Jezeli nie zostala przedtem
// przeslana komenda 'PASV' to nie ma zadnego wolnego watku i zostaje zwrocony pusty wskaznik
DataThread *CmdThread::getUnusedThread() {
    if(unusedDataThreads.size() == 0)
        return nullptr;

    DataThread *dataThread = unusedDataThreads[0];
    unusedDataThreads.erase(unusedDataThreads.begin());

    usedDataThreads.push_back(dataThread);

    return dataThread;
}

// funkcja obslugujaca zmiane sciezki przez klienta. Zaklada, ze najnizszy poziom sciezki to /home/USER
bool CmdThread::changeDir(string newPath) {
    string convertedPath = convertPath(newPath);

    // sprawdzenie czy ma przynajmniej dwa '/'. Minimalna sciezka to /home/USER
    int counter = 0;
    string::size_type pos = 0;
    while(pos != string::npos) {
        pos = convertedPath.find('/', pos+1);
        counter++;
    }

    if(counter >= 2) {
        try {
            getResultOfSystemCommand(login, "if test -d \"" + convertedPath + "\"; then exit 0; else exit 1; fi", false);
        } catch(exception &) {
            return false;
        }

        path = convertedPath;
        return true;
    } else
        return false;
}

// funkcja przyjmujaca wiadomosci od uzytkownia. Dzieli otrzymany komunikat na polecenie oraz jej parametry
// oraz wykrywa zerwanie polaczenia przez klienta
bool CmdThread::receiveMessage() {
    receivedCommand.clear();
    int howMany = -1;
    while(!(receivedCommand.find("\r\n") != string::npos || howMany == 0)) {
        bzero(buffor, BUFOR_SIZE);
        howMany = read(socket, buffor, BUFOR_SIZE);
        receivedCommand += buffor;
    }

    // utracenie polaczenia
    if(howMany == 0)
        return false;

    // wyselekcjonowanie komendy i parametru
    string::size_type pos = receivedCommand.find(" ");
    if(pos != string::npos) {
        receivedParameter = receivedCommand.substr(pos+1, receivedCommand.length()-pos-1-2);
        receivedCommand = receivedCommand.substr(0, pos);

        log(socket, "read: command - " + receivedCommand + ", parameter - " + receivedParameter);
    } else {
        receivedCommand = receivedCommand.substr(0, receivedCommand.length()-2);
        log(socket, "read: command - " + receivedCommand);
    }

    return true;
}

void CmdThread::log(int socket, string message) {
    cout << "LOG: Client " << socket << " - " << message << endl;
}

// funkcja, ktora wysyla wiadomosc do klienta
void CmdThread::writeAndLog(int socket, string message){
    string toWrite = message + END_OF_STRING;
    write(socket, toWrite.c_str(), toWrite.length());
    cout << "LOG: Client " << socket << " - write: " << message << endl;
}

// funkcja zajmujaca sie nawigacja po katalogach
string CmdThread::convertPath(string receivedPath) {
    size_t pos = receivedPath.find_last_of('/');

    // folder w gore
    if(receivedPath == "..") {
        size_t up = path.substr(0, pos).find_last_of('/');

        if(up == 0)
            return "/";
        else
            return path.substr(0, up);
    }

    if(pos != string::npos)
        return receivedPath;
    else
        return path + "/" + receivedPath;
}

// funkcja selekcjonujaca nazwe pliku z pelnej sciezki. Wykrywa przypadek, ze
// parametrem jest sama nazwa pliku
string CmdThread::fileWithoutPath(string file) {
    size_t pos = file.find_last_of('/');

    if(pos == string::npos)
        return file;
    else
        return file.substr(pos+1);
}

// funkcja tworzaca pelna sciezke z samej nazwy pliku. Wykrywa przypadek, ze
// parametrem jest juz pelne sciezka
string CmdThread::filePath(string file) {
    size_t pos = file.find('/');

    if(pos == string::npos)
        return path + "/" + file;
    else
        return file;
}
