#ifndef CLIENTSERVER_H
#define CLIENTSERVER_H

#define END_OF_STRING "\r\n"
#define END_OF_STRING_LINUX "\r"
#define MAX_TRIES 10
#define PORT_EPHERMAL_LOW 49152
#define PORT_EPHERMAL_HIGH 65535
#define BUFOR_SIZE 1024

#include <fstream>
#include <boost/algorithm/string.hpp>
#include <pthread.h>
#include "server.h"
#include "utilityfunctions.h"

enum TYPE { UNKNOWN, ASCII, BINARY };
static string enumStrings[] = { "UNKNOWN", "ASCII", "BINARY" };

class DataThread : public Server
{
public:
    DataThread();
    ~DataThread();

    bool isRunning();
    bool isError();
    string getCommand();

    pthread_cond_t *getCondWaitingForEnd();
    pthread_mutex_t *getMutexWaitingForEnd();
    void setAbor(bool abor);

    void executeAndWrite(const string &command, const string &fileName, enum TYPE typ, const string &path, int offset);
    void executeAndWrite(const string &command, const string &parameter, enum TYPE typ, const string &path, const string &login);
private:
    pthread_t threadId;
    int dataSocket;

    pthread_cond_t condWaitingForEnd;
    pthread_mutex_t mutexWaitingForEnd, mutexStopTransfer;

    bool running;
    bool error;
    bool abor;

    string login;
    string command;
    string commandParameterOrFileName;
    TYPE typ;
    string path;
    int offset;

    static void *thread(void* arg);
    static void commandLIST(DataThread *dt, const string &login);

    static void sendFile(int dataSocket, string filePath, TYPE typ, int offset, bool &abor, pthread_mutex_t &mutexStopTransfer, bool &error);
    static void receiveFile(int dataSocket, string filePath, TYPE typ, int offset, bool &abor, pthread_mutex_t &mutexStopTransfer, bool &error);
};

#endif // CLIENTSERVER_H
