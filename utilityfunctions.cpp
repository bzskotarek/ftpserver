#include "utilityfunctions.h"

mt19937 generator;

int generateValue(int min, int max) {
    uniform_int_distribution<int> distribution = uniform_int_distribution<>(min, max);
    return distribution(generator);
}

// pobieranie adresu IP dla aktywnego interfejsu
string getMachineAddress() {
    struct ifaddrs *ifAddrStruct = NULL;
       struct ifaddrs *ifa = NULL;
       void *tmpAddrPtr = NULL;

       getifaddrs(&ifAddrStruct);

       for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
           if (!ifa->ifa_addr) {
               continue;
           }
           if (ifa->ifa_addr->sa_family == AF_INET) { // check it is IP4
               // is a valid IP4 Address
               tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
               char addressBuffer[INET_ADDRSTRLEN];
               inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
               //printf("%s IP Address %s\n", ifa->ifa_name, addressBuffer);
               string address(addressBuffer);
               if(address != "127.0.0.1")
                   return address;
           }
       }
       if (ifAddrStruct!=NULL) freeifaddrs(ifAddrStruct);

       return "";
}

// funkcja wykonujaca polecenie z uprawnieniami zalogowanego uzytkownika oraz zwracajaca jej rezultat. W przypadku wystapienia bledu rzuca wyjatek
string getResultOfSystemCommand(const string &user, /*const string user, const string password,*/ const string &command, bool addEndOfString) {
    string result("");

    cout << "LOG: Command: " << command << endl;

    int number = 0;

    bool czy = false;
    while(!czy) {
        string commandAndTmp = SUDO + "su - " + user + " -c '" + command + " > /tmp/ftpsimple" + to_string(number) + ".txt" + "'";
        int result = system(commandAndTmp.c_str());
        if(result != 0)
            number++;
        else
            czy = true;
    }

    ifstream file;

    file.open("/tmp/ftpsimple" + to_string(number) + ".txt");
    if(!file.is_open())
        throw exception();

    string line;
    while(getline(file, line)) {
        if(addEndOfString == true)
            result += line + END_OF_STRING;
        else
            result += line;
    }

    file.close();
    string name = "/tmp/ftpsimple" + to_string(number) + ".txt";
    remove(name.c_str());

    return result;
}

// funkcja weryfikujaca poprawnosc hasla dla przeslanego uzytkownika
bool isPasswordCorrect(const string &user, const string &password) {
    // pobieranie linii danego uzytkownika
    int number = 0;

    bool czy = false;
    while(!czy) {
        string grep = SUDO + "cat /etc/shadow | grep " + user + " > /tmp/ftpsimple" + to_string(number) + ".txt";
        if(system(grep.c_str()) != 0)
            number++;
        else
            czy = true;
    }

    ifstream file;
    file.open("/tmp/ftpsimple" + to_string(number) + ".txt");
    if(!file.is_open())
        throw exception();

    // wyodrebnianie funkcji haszujacej
    string passwordHash;
    string fragment;
    string salt = "$";
    for(int i = 0; getline(file, fragment, '$'); i++) {
        if(i > 0 && i < 3) {
            salt += fragment + "$";
        } else if(i == 3) {
            passwordHash = fragment;
            passwordHash = passwordHash.substr(0, passwordHash.find_first_of(':'));
        }
    }
    file.close();
    string name = "/tmp/ftpsimple" + to_string(number) + ".txt";
    remove(name.c_str());

    // utworzenie sumy kontrolnej na podstawie hasla i funkcji haszujacej
    char *hash = crypt(password.c_str(), salt.c_str());

    // wyryfikowanie poprawnosci
    if( (salt + passwordHash) == hash)
        return true;
    else
        return false;
}

bool isNumber(const string &s) {
    string::const_iterator it = s.begin();

    while (it != s.end() && isdigit(*it)) ++it;

    return !s.empty() && it == s.end();
}
