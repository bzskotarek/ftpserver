#ifndef CLIENTTHREAD_H
#define CLIENTTHREAD_H

#define END_OF_STRING "\r\n"
#define BUFOR_SIZE 1024

#include <stdlib.h>

#include <pthread.h>
#include <vector>
#include <string>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include "datathread.h"
#include "utilityfunctions.h"

using namespace std;

struct WaitingForEndArgument {
    int *cmdSocket;
    DataThread *dataThread;
    pthread_mutex_t *mutexWaitingForEnd;
    pthread_cond_t *condWaitingForEnd;
};

struct TimeoutGuardianArgument {
    int *counter;
    pthread_mutex_t *mutexCounter;
    int socket;
    pthread_t threadId;
};

class CmdThread
{
public:
    CmdThread(int socket);
    ~CmdThread();
    pthread_t runThreads();

    static int timeoutLimit;

private:
    static vector<DataThread*> unusedDataThreads, usedDataThreads;

    int socket;
    pthread_t threadID;
    int counter;
    pthread_mutex_t mutexCounter;

    pthread_t threadWaitingForEndID;
    pthread_t threadTimeoutGuardianID;

    bool loginIn;
    string login;
    string path;
    string receivedCommand, receivedParameter;
    string toRename;
    string toWrite;
    TYPE type;
    int REST;

    char buffor[BUFOR_SIZE];

    void notLoggedIn();
    void loggedIn();
    DataThread *getPASV();
    void addDataThread();
    DataThread *getUnusedThread();

    bool changeDir(string newPath);
    string convertPath(string receivedPath);
    string fileWithoutPath(string file);
    string filePath(string file);

    bool receiveMessage();

    static void *thread(void* arg);
    static void *threadWaitingForEndOfDataThread(void *arg);
    static void *threadTimeoutGuardian(void *arg);

    static void writeAndLog(int socket, string message);
    static void log(int socket, string message);
};

#endif // CLIENTTHREAD_H
